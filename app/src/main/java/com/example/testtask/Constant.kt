package com.example.testtask

class Constant {
    companion object{
        const val DATABASE_NAME = "database_calories_app.realm"
        const val uid ="uid"
        const val identity ="identity"
        const val BEARER ="Bearer "
    }
}