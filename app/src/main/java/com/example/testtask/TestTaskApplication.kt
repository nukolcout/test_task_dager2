package com.example.testtask

import android.app.Application
import android.content.res.Resources
import com.example.testtask.Constant.Companion.DATABASE_NAME
import com.example.testtask.di.AppComponent
import com.example.testtask.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*
import javax.inject.Inject

class TestTaskApplication: Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    private var locale: Locale? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = AppInjector.init(this)
        appResources = resources
        Realm.init(this)
        val config = RealmConfiguration.Builder().name(DATABASE_NAME).build()
        Realm.setDefaultConfiguration(config)
    }

    override fun androidInjector() = dispatchingAndroidInjector

    companion object {
        lateinit var appComponent: AppComponent
        lateinit var appResources: Resources
    }

}