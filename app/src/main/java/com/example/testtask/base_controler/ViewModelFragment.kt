package com.example.testtask.base_controler

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.example.testtask.R
import com.example.testtask.di.Injectable
import com.google.android.material.snackbar.Snackbar
import io.realm.internal.Util
import java.util.*
import javax.inject.Inject

abstract class ViewModelFragment<V : ViewBinding, VM : ViewModel> : NavFragment<V>(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    protected lateinit var viewModel: VM
    private var locale: Locale? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = createViewModel()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    open fun showError(message: String?, ok: (() -> Unit)? = null) {
        message?.run {
            val snackbar =
                Snackbar.make(binding.root, message, Snackbar.LENGTH_INDEFINITE)
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.setAction(R.string.ok) { _ ->
                snackbar.dismiss()
                ok?.invoke()
            }
            snackbar.show()
        }
    }

    abstract fun createViewModel(): VM

}