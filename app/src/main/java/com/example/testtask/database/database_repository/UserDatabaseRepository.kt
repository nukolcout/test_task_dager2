package com.example.testtask.database.database_repository

import com.example.testtask.database.model.UserInfoDB
import io.reactivex.Single
import io.realm.Realm
import io.realm.kotlin.where

class UserDatabaseRepository : BaseRepository<UserInfoDB>() {
    val realm: Realm = Realm.getDefaultInstance()

    override fun insert(item: UserInfoDB): String {
        realm.executeTransactionAsync { realm ->
            realm.insert(item)
        }
        return item.id
    }

    override fun insertList(items: List<UserInfoDB>): List<String> {
        realm.insert(items)
        val listIds = mutableListOf<String>()
        items.forEach {
            listIds.add(it.id)
        }
        return listIds
    }

    override fun isEmpty(): Boolean {
        val items = realm.where<UserInfoDB>().findAll()
        return items.isEmpty()
    }

    override fun getItemById(itemId: String): UserInfoDB? {
        return realm.where<UserInfoDB>().equalTo("id", itemId).findFirst()
    }

    fun isItemById(itemId: String): Boolean {
        return realm.where<UserInfoDB>().equalTo("id", itemId).findFirst() != null
    }

    override fun getAll(): List<UserInfoDB> {
        return realm.where<UserInfoDB>().findAll()
    }

    override fun deleteById(itemId: String): Single<Boolean> {
        return Single.create { emitter ->
            try {
                val food = realm.where<UserInfoDB>().equalTo("id", itemId).findFirst()
                if (food != null) {
                    food.deleteFromRealm()
                    emitter.onSuccess(true)
                } else {
                    emitter.onSuccess(false)
                }
            } catch (ex: Throwable) {
                emitter.onError(ex)
            }
        }
    }

    override fun delete(item: UserInfoDB): Single<String> {
        return Single.create { emitter ->
            try {
                item.deleteFromRealm()
                emitter.onSuccess(item.id)
            } catch (ex: Throwable) {
                emitter.onError(ex)
            }
        }
    }

    override fun update(item: UserInfoDB): Single<UserInfoDB> {
        return Single.create { emitter ->
            try {
                realm.insertOrUpdate(item)
                emitter.onSuccess(item)
            } catch (ex: Throwable) {
                emitter.onError(ex)
            }
        }
    }
}