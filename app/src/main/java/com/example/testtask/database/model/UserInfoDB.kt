package com.example.testtask.database.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UserInfoDB constructor(
    @PrimaryKey var id: String = "",
    var age: Int? = 0,
    var country: String? = "",
    var firstName: String? = "",
    var gender: String? = "",
    var lastName: String? = ""
) : RealmObject()