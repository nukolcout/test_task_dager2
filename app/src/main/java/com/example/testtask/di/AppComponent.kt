package com.example.testtask.di

import android.app.Application
import com.example.testtask.TestTaskApplication
import com.example.testtask.di.module.ApiModule
import com.example.testtask.di.module.AppModule
import com.example.testtask.di.module.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ApiModule::class,
        MainActivityModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: TestTaskApplication)
}