package com.example.testtask.di.module

import com.example.testtask.net.NetService
import com.example.testtask.net.api.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {
    @Singleton
    @Provides
    fun provideUser(retrofit: Retrofit): UserApi = retrofit.create(UserApi::class.java)

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = NetService.getClient()
}