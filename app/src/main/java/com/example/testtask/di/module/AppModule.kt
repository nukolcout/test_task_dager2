package com.example.testtask.di.module

import android.app.Application
import android.content.Context
import com.example.testtask.database.database_repository.UserDatabaseRepository
import com.example.testtask.net.data_event.DataEventHelper
import com.example.testtask.net.data_event.DataEventHelperImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: Application): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideUserUserDatabaseRepository(): UserDatabaseRepository = UserDatabaseRepository()

    @Provides
    fun provideDataEventHelper(dataEvent: DataEventHelperImpl): DataEventHelper = dataEvent

}