package com.example.testtask.di.module

import com.example.testtask.ui.user_info.DescriptionUserFragment
import com.example.testtask.ui.user_list.ListUserFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeListUserFragment(): ListUserFragment

    @ContributesAndroidInjector
    abstract fun contributeDescriptionUserFragment(): DescriptionUserFragment
}