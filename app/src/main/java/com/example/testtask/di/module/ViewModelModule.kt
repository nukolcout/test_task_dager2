package com.example.testtask.di.module

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testtask.di.ViewModelFactory
import com.example.testtask.di.ViewModelKey
import com.example.testtask.ui.user_info.DescriptionViewModel
import com.example.testtask.ui.user_list.ListUserViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ListUserViewModel::class)
    fun bindListUserViewModel(viewModel: ListUserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DescriptionViewModel::class)
    fun bindDescriptionViewModel(viewModel: DescriptionViewModel): ViewModel

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}