package com.example.testtask.net

import com.google.gson.annotations.Expose

class DataEvent<T>(val event: Event, @Expose val data: T?, val errorMessage: String?) {

    companion object {

        fun <T> loading(): DataEvent<T> {
            return DataEvent(Event.LOADING, null, null)
        }

        fun <T> success(data: T?): DataEvent<T> {
            return DataEvent(Event.SUCCESS, data, null)
        }

        fun <T> error(errorMessage: String?): DataEvent<T> {
            return DataEvent(Event.ERROR, null, errorMessage)
        }
    }

    fun whatIf(actionsOn: OnDataEventListener<T>) {
        when (this.event) {
            Event.LOADING -> actionsOn.onLoading()
            Event.SUCCESS -> actionsOn.onSuccess(this.data)
            Event.ERROR -> actionsOn.onError(this.errorMessage)
        }
    }


    enum class Event {
        LOADING,
        SUCCESS,
        ERROR
    }
}