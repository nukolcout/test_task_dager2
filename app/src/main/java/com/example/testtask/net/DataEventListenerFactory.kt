package com.example.testtask.net

import android.view.View
import com.example.testtask.utils.hide
import com.example.testtask.utils.show

class DataEventListenerFactory {
    companion object {

        fun <T : Any?> buildListener(
            loadingViews: View? = null,
            onLoading: (() -> Unit)? = null,
            onSuccess: ((data: T) -> Unit)? = null,
            onError: ((errorMessage: String?) -> Unit)? = null
        ): OnDataEventListener<T> {

            return object : OnDataEventListener<T> {

                override fun onLoading() {
                    loadingViews?.show()
                    onLoading?.invoke()
                }

                override fun onSuccess(data: T?) {
                    loadingViews?.hide()
                    if (data != null) {
                        onSuccess?.invoke(data)
                    }
                }

                override fun onError(errorMessage: String?) {
                    loadingViews?.hide()
                    onError?.invoke(errorMessage ?: "")
                }
            }
        }
    }
}