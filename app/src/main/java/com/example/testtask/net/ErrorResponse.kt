package com.example.testtask.net

data class ErrorResponse(
    val error_description: String,
    val causes: Map<String, String> = emptyMap()
)