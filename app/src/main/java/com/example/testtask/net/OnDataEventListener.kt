package com.example.testtask.net

interface OnDataEventListener <T> {

    fun onLoading()

    fun onSuccess(data: T?)

    fun onError(errorMessage: String?)
}