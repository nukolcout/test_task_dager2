package com.example.testtask.net

enum class StatusApi(val status: String) {
    SUCCEESS("success"),
    ERROR("error")
}