package com.example.testtask.net.api

import android.database.Observable
import com.example.testtask.net.model.response.GetUserInfoResponse
import com.example.testtask.net.model.response.ListUserResponse
import com.example.testtask.utils.GenerationJWT
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface UserApi {

    @GET("list")
    suspend fun getListUser(): ListUserResponse

    @GET("get/{id}")
    suspend fun getUserInfo(@Path("id") id: String): GetUserInfoResponse

}