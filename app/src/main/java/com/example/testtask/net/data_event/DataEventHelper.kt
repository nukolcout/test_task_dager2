package com.example.testtask.net.data_event

import androidx.lifecycle.MutableLiveData
import com.example.testtask.net.DataEvent
import com.example.testtask.net.model.response.ApiResponse
import retrofit2.HttpException

interface DataEventHelper {
    fun <T : ApiResponse> notifyApiResult(model: T, liveData: MutableLiveData<DataEvent<T>>)
}