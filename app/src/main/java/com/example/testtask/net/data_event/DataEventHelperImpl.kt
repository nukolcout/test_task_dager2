package com.example.testtask.net.data_event

import androidx.lifecycle.MutableLiveData
import com.example.testtask.net.DataEvent
import com.example.testtask.net.StatusApi
import com.example.testtask.net.model.response.ApiResponse
import com.google.gson.Gson
import retrofit2.HttpException
import javax.inject.Inject

class DataEventHelperImpl @Inject constructor() : DataEventHelper {

    override fun <T : ApiResponse> notifyApiResult(
        model: T,
        liveData: MutableLiveData<DataEvent<T>>
    ) {
        when (model.status) {
            StatusApi.ERROR.status -> {
                liveData.postValue(DataEvent.error(getError(model)))
            }
            StatusApi.SUCCEESS.status -> {
                liveData.postValue(DataEvent.success(model))
            }
            else -> {
                liveData.postValue(DataEvent.error(getError(model)))
            }
        }
    }

    private fun <T : ApiResponse> getError(model: T): String? =
        model.error?.message ?: model.status


    inline fun <reified T : ApiResponse> checkError(
        ex: HttpException,
        liveData: MutableLiveData<DataEvent<T>>
    ) {
        val error = ex.response()?.errorBody()!!.charStream().readText()
        val date = Gson().fromJson(error, T::class.java)
        notifyApiResult(date, liveData)
    }


}