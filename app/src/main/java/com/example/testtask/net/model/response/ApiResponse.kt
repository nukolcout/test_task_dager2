package com.example.testtask.net.model.response

import com.google.gson.annotations.SerializedName

  open class ApiResponse {
    @SerializedName("status")
    var status: String? = ""

    @SerializedName("error")
    val error: Error? = null
}