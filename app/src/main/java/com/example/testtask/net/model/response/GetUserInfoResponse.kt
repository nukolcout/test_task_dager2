package com.example.testtask.net.model.response


import com.google.gson.annotations.SerializedName

data class GetUserInfoResponse(
    @SerializedName("data")
    var userInfo: UserInfo? = null
) : ApiResponse()