package com.example.testtask.net.model.response


import com.google.gson.annotations.SerializedName

data class ListUserResponse(
    @SerializedName("data")
    var userList: List<String>
) : ApiResponse()