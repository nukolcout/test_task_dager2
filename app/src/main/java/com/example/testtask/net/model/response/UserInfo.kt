package com.example.testtask.net.model.response


import com.example.testtask.database.model.UserInfoDB
import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("age")
    val age: Int? = null,
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("firstName")
    val firstName: String? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("lastName")
    val lastName: String? = null
)

fun UserInfo.toDbUser(): UserInfoDB {
    val userInfoDB = UserInfoDB()
    userInfoDB.age = age
    userInfoDB.country = country
    userInfoDB.firstName = firstName
    userInfoDB.lastName = lastName
    userInfoDB.id = id ?: ""
    userInfoDB.gender = gender
    return userInfoDB
}