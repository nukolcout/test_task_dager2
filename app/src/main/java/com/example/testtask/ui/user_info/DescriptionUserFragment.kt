package com.example.testtask.ui.user_info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.testtask.base_controler.ViewModelFragment
import com.example.testtask.database.model.UserInfoDB
import com.example.testtask.databinding.FragmentDescriptionUserBinding
import com.example.testtask.di.injectViewModel

class DescriptionUserFragment :
    ViewModelFragment<FragmentDescriptionUserBinding, DescriptionViewModel>() {
    private val args by navArgs<DescriptionUserFragmentArgs>()

    override fun createViewModel(): DescriptionViewModel =
        injectViewModel(viewModelStore, viewModelFactory)


    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDescriptionUserBinding =
        FragmentDescriptionUserBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getArgument()
        intViewModel()
    }

    private fun intViewModel() {
        viewModel.getUserObservable().observe(viewLifecycleOwner, { setUser(it) })
    }

    private fun getArgument() {
        if (!args.id.isNullOrEmpty()) {
            args.id?.run { viewModel.getUser(this) }
        }
    }

    private fun setUser(userInfoDB: UserInfoDB) {
        binding.user = userInfoDB
        binding.executePendingBindings()
    }

}