package com.example.testtask.ui.user_info

import androidx.lifecycle.MutableLiveData
import com.example.testtask.base_controler.BaseViewModel
import com.example.testtask.database.database_repository.UserDatabaseRepository
import com.example.testtask.database.model.UserInfoDB
import javax.inject.Inject

class DescriptionViewModel @Inject constructor(
    private val userDatabaseRepository: UserDatabaseRepository
) : BaseViewModel() {
    private val userInfoDB = MutableLiveData<UserInfoDB>()

    fun getUserObservable(): MutableLiveData<UserInfoDB> {
        return userInfoDB
    }

    fun getUser(id: String) {
        getUserObservable().postValue(userDatabaseRepository.getItemById(id))
    }

}