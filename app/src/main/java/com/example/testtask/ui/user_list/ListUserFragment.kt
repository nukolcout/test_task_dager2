package com.example.testtask.ui.user_list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.testtask.R
import com.example.testtask.base_controler.ViewModelFragment
import com.example.testtask.databinding.FragmentListUserBinding
import com.example.testtask.di.injectViewModel
import com.example.testtask.net.DataEventListenerFactory
import com.example.testtask.net.model.response.GetUserInfoResponse
import com.example.testtask.net.model.response.ListUserResponse
import com.example.testtask.net.model.response.UserInfo
import com.example.testtask.net.model.response.toDbUser
import com.example.testtask.ui.user_list.adapter.UserAdapter

class ListUserFragment : ViewModelFragment<FragmentListUserBinding, ListUserViewModel>() {
    private var userList = ArrayList<UserInfo>()
    private val userAdapter = UserAdapter().apply {
        clickItem =
            {
                findNavController().navigate(
                    ListUserFragmentDirections.actionToDescriptionUserFragment(
                        it.id
                    )
                )
            }
    }

    override fun createViewModel(): ListUserViewModel =
        injectViewModel(viewModelStore, viewModelFactory)

    private val userListDataListener by lazy {
        DataEventListenerFactory.buildListener<ListUserResponse>(
            loadingViews = binding.pgLoad,
            onSuccess = { viewModel.loadUserInfo(it) },
            onError = {
                showError(
                    String.format(getString(R.string.error_on_server), it),
                    ok = { viewModel.getAllUser() })
            }
        )
    }

    private val userInfoDataListener by lazy {
        DataEventListenerFactory.buildListener<GetUserInfoResponse>(
            onSuccess = {
                processingUsing(it)

            },
            onError = { Log.e(ListUserFragment::class.java.name, it.toString()) }
        )
    }

    private fun processingUsing(userDescription: GetUserInfoResponse) {
        userDescription.userInfo?.run {
            userList.add(this)
            viewModel.saveUser(this.toDbUser())
        }
        userAdapter.items = userList
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListUserBinding =
        FragmentListUserBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initViewModel()
    }

    private fun initViewModel() {
        with(viewModel) {
            if (isListEmpty()) {
                getUserObservable().observe(viewLifecycleOwner,
                    { userList -> userList.whatIf(userListDataListener) })

                getDescriptionUserResponseObservable().observe(viewLifecycleOwner,
                    { userInfo -> userInfo.whatIf(userInfoDataListener) })

                getAllUser()
            }
        }
    }

    private fun initAdapter() {
        binding.rvUser.adapter = userAdapter
    }

}