package com.example.testtask.ui.user_list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.testtask.base_controler.BaseViewModel
import com.example.testtask.database.database_repository.UserDatabaseRepository
import com.example.testtask.database.model.UserInfoDB
import com.example.testtask.net.DataEvent
import com.example.testtask.net.api.UserApi
import com.example.testtask.net.data_event.DataEventHelperImpl
import com.example.testtask.net.model.response.GetUserInfoResponse
import com.example.testtask.net.model.response.ListUserResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

class ListUserViewModel @Inject constructor(
    private val userApi: UserApi,
    private val userDatabaseRepository: UserDatabaseRepository,
    private val dataEventHelperImpl: DataEventHelperImpl
) : BaseViewModel() {
    private val userResponse = MutableLiveData<DataEvent<ListUserResponse>>()
    private val descriptionUserResponseResponse = MutableLiveData<DataEvent<GetUserInfoResponse>>()


    fun getUserObservable(): MutableLiveData<DataEvent<ListUserResponse>> {
        return userResponse
    }

    fun getDescriptionUserResponseObservable(): MutableLiveData<DataEvent<GetUserInfoResponse>> {
        return descriptionUserResponseResponse
    }

    fun getAllUser() {
        getUserObservable().postValue(DataEvent.loading())
        viewModelScope.launch {
            try {
                val date = userApi.getListUser()
                dataEventHelperImpl.notifyApiResult(date, getUserObservable())
            } catch (ex: HttpException) {
                dataEventHelperImpl.checkError(ex, getUserObservable())
            }
        }
    }

    fun loadUserInfo(listUserResponse: ListUserResponse) {
        getDescriptionUserResponseObservable().postValue(DataEvent.loading())
        viewModelScope.launch {
            for (id in listUserResponse.userList) {
                try {
                    val date = userApi.getUserInfo(id = id)
                    dataEventHelperImpl.notifyApiResult(
                        date,
                        getDescriptionUserResponseObservable()
                    )
                } catch (ex: HttpException) {
                    dataEventHelperImpl.checkError(ex, getDescriptionUserResponseObservable())
                }
            }
        }
    }

    fun saveUser(userInfoDB: UserInfoDB): String? {
        if (userInfoDB.id.isNotEmpty()) {
            val user = userDatabaseRepository.isItemById(userInfoDB.id)
            if (!user) {
                return userDatabaseRepository.insert(userInfoDB)
            }
        }
        return null
    }

    fun isListEmpty(): Boolean {
        return getUserObservable().value?.data?.userList?.isEmpty() ?: true
    }
}

