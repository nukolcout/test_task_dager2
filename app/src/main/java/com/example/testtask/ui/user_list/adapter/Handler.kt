package com.example.testtask.ui.user_list.adapter

import com.example.testtask.net.model.response.UserInfo

interface Handler {
    fun onClick(userInfo: UserInfo)
}