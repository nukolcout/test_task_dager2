package com.example.testtask.ui.user_list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testtask.databinding.ListItemUserBinding
import com.example.testtask.net.model.response.UserInfo

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    var clickItem: ((idUser: UserInfo) -> Unit)? = null

    var items = mutableListOf<UserInfo>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemUserBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val menuItem = items[position]
        holder.bind(menuItem)
    }

    inner class ViewHolder(private val binding: ListItemUserBinding) :
        RecyclerView.ViewHolder(binding.root), Handler {
        fun bind(model: UserInfo) {
            binding.model = model
            binding.handler = this
            binding.executePendingBindings()
        }

        override fun onClick(userInfo: UserInfo) {
            clickItem?.invoke(userInfo)

        }
    }

}