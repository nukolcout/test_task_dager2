package com.example.testtask.utils

import android.util.Base64
import com.example.testtask.BuildConfig.secretKey
import com.example.testtask.Constant.Companion.BEARER
import com.example.testtask.Constant.Companion.identity
import com.example.testtask.Constant.Companion.uid
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm

object GenerationJWT {

    fun getJwt(): String =
        BEARER + Jwts.builder().claim(uid, "").claim(identity, "")
            .signWith(SignatureAlgorithm.HS256, getSecretKey().toByteArray())
            .compact()

    private fun getSecretKey() =
        Base64.encodeToString(secretKey.toByteArray(), Base64.DEFAULT).trim()

}