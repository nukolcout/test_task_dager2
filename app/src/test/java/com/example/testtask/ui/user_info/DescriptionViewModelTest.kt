package com.example.testtask.ui.user_info

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.testtask.database.database_repository.UserDatabaseRepository
import com.example.testtask.database.model.UserInfoDB
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class DescriptionViewModelTest {
    @get:Rule
    val mActivityRule = InstantTaskExecutorRule()

    @Mock
    lateinit var userDatabaseRepository: UserDatabaseRepository

    @Mock
    lateinit var viewModel: DescriptionViewModel

    @Mock
    lateinit var userInfoDB: Observer<UserInfoDB>

    private var userInfoDBTest = UserInfoDB(id = "1")
    private var USER_ID_1 = "1"
    private var USER_ID_2 = "2"
    private lateinit var successDataEvent: UserInfoDB


    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        viewModel = DescriptionViewModel(userDatabaseRepository)
        viewModel.getUserObservable().observeForever(userInfoDB)

    }

    @Test
    fun getUserFound() {
        `when`(viewModel.getUser(USER_ID_1)).then {
            userInfoDB.onChanged(userInfoDBTest)
        }
        viewModel.getUser(USER_ID_1)
        verify(userInfoDB).onChanged(userInfoDBTest)
    }
    @Test

    fun getUserNotFound() {
        `when`(viewModel.getUser(USER_ID_1)).then {
            userInfoDB.onChanged(userInfoDBTest)
        }
        viewModel.getUser(USER_ID_2)
        verify(userInfoDB).onChanged(userInfoDBTest)
    }


}