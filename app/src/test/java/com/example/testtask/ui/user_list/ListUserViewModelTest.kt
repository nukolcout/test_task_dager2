package com.example.testtask.ui.user_list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.testtask.database.database_repository.UserDatabaseRepository
import com.example.testtask.database.model.UserInfoDB
import com.example.testtask.net.DataEvent
import com.example.testtask.net.StatusApi
import com.example.testtask.net.api.UserApi
import com.example.testtask.net.data_event.DataEventHelperImpl
import com.example.testtask.net.model.response.GetUserInfoResponse
import com.example.testtask.net.model.response.ListUserResponse
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class ListUserViewModelTest {

    @get:Rule
    val mActivityRule = InstantTaskExecutorRule()

    @Mock
    lateinit var userDatabaseRepository: UserDatabaseRepository

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @Mock
    lateinit var userApi: UserApi
    lateinit var viewModel: ListUserViewModel

    @Mock
    lateinit var dataEventHelperImpl: DataEventHelperImpl

    @Mock
    lateinit var userResponseTest: Observer<DataEvent<ListUserResponse>>

    @Mock
    lateinit var userInfoResponseTest: Observer<DataEvent<GetUserInfoResponse>>

    private var listUserResponse = ListUserResponse(arrayListOf())
    private lateinit var successDataEvent: DataEvent<ListUserResponse>

    var getUserInfoResponse = GetUserInfoResponse()
    private lateinit var successDataEventUserInfoResponse: DataEvent<GetUserInfoResponse>

    private var userInfoDB = UserInfoDB(id = "2")
    private var USER_ID_1 = "1"
    private var USER_ID_2 = "2"
    private var IS_SELECTED = true

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = ListUserViewModel(userApi, userDatabaseRepository, dataEventHelperImpl)
    }


    @Test
    fun getAllUser() = testScope.runBlockingTest {
        setUserAllMock()
        viewModel.getAllUser()
        verify(userResponseTest).onChanged(successDataEvent)
    }

    private fun setUserAllMock() {
        viewModel.getUserObservable().observeForever(userResponseTest)
        listUserResponse.status = StatusApi.SUCCEESS.status
        successDataEvent = DataEvent.success(listUserResponse)
        `when`(
            dataEventHelperImpl.notifyApiResult(
                listUserResponse,
                viewModel.getUserObservable()
            )
        ).then {
            viewModel.getUserObservable().postValue(successDataEvent)
        }

        runBlocking {
            launch(Dispatchers.IO) {
                `when`(userApi.getListUser()).thenReturn(listUserResponse)
            }
        }.start()
    }


    @Test
    fun loadUserInfo() = testScope.runBlockingTest {
        setLoadUserInfoMock()
        viewModel.loadUserInfo(listUserResponse)
        verify(userInfoResponseTest).onChanged(successDataEventUserInfoResponse)
    }

    private fun setLoadUserInfoMock() {
        viewModel.getDescriptionUserResponseObservable().observeForever(userInfoResponseTest)
        getUserInfoResponse.status = StatusApi.SUCCEESS.status
        successDataEventUserInfoResponse = DataEvent.success(getUserInfoResponse)


        `when`(
            dataEventHelperImpl.notifyApiResult(
                getUserInfoResponse,
                viewModel.getDescriptionUserResponseObservable()
            )
        ).then {
            viewModel.getDescriptionUserResponseObservable()
                .postValue(successDataEventUserInfoResponse)
        }

        runBlocking {
            launch(Dispatchers.IO) {
                `when`(userApi.getUserInfo("")).thenReturn(getUserInfoResponse)
            }
        }.start()

        listUserResponse.userList = arrayListOf()
        (listUserResponse.userList as ArrayList<String>).add("")
    }


    @Test
    fun saveUser() {
        `when`(userDatabaseRepository.insert(userInfoDB)).thenReturn(USER_ID_1)
        assertEquals(userDatabaseRepository.insert(userInfoDB), USER_ID_1)
    }

    @Test
    fun selectUser() {
        `when`(userDatabaseRepository.isItemById(USER_ID_1)).thenReturn(IS_SELECTED)
        assertTrue(userDatabaseRepository.isItemById(USER_ID_1))
    }

    @Test
    fun notFoundUser() {
        `when`(userDatabaseRepository.isItemById(USER_ID_1)).thenReturn(!IS_SELECTED)
        assertFalse(userDatabaseRepository.isItemById(USER_ID_1))
    }

    @Test
    fun saveUserTrue() {
        `when`(userDatabaseRepository.isItemById(USER_ID_1)).thenReturn(!IS_SELECTED)
        `when`(userDatabaseRepository.insert(userInfoDB)).thenReturn(USER_ID_2)
        assertEquals(viewModel.saveUser(userInfoDB), USER_ID_2)
    }

    @Test
    fun saveUserFalse() {
        `when`(userDatabaseRepository.isItemById("2")).thenReturn(IS_SELECTED)
        `when`(userDatabaseRepository.insert(userInfoDB)).thenReturn(USER_ID_2)
        assertEquals(viewModel.saveUser(userInfoDB), null)
    }

    @Test
    fun isListEmptyTrue() {
        viewModel.getUserObservable().observeForever(userResponseTest)
        viewModel.getUserObservable().postValue(null)
        viewModel.isListEmpty()
        assertEquals(  viewModel.isListEmpty(), true)
    }
}